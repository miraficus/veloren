## Regeneration
buff-title-heal = Léčba
buff-desc-heal = Přidá život během určitého času.
buff-stat-health = Obnoví { $str_total } Životů
## Potion
buff-title-potion = Lektvar
buff-desc-potion = Piju...
## Saturation
buff-title-saturation = Nasycení
buff-desc-saturation = Přídá život během času ze Spotřebních.
## Campfire
buff-title-campfire_heal = Léčba Táborákem
buff-desc-campfire_heal = Odpočinek u ohně léčí { $rate }% za sekundu.
## Energy Regen
buff-title-energy_regen = Regenerace Energie
buff-desc-energy_regen = Rychlejší regenerace energie
buff-stat-energy_regen = obnovuje { $str_total } Energie
## Health Increase
buff-stat-increase_max_energy = Zvedne Maximalní Výdrž o { $strength }
buff-desc-increase_max_health = Zvedne maximální limit životů
buff-stat-increase_max_health = 
    Zvedne Maximální Počet Životů
    o { $strength }
## Energy Increase
buff-title-increase_max_energy = Zvedne Maximální Energii
buff-desc-increase_max_energy = Zvedne maximální limit energie
buff-stat-increase_max_energy =
    Zvedne Maximální Počet Energie
    o { $strength }
## Invulnerability
buff-title-invulnerability = Nezranitelnost
buff-desc-invulnerability = Žádný útok tě nezraní.
buff-stat-invulnerability = Zaručuje nezranitelnost
## Protection Ward
buff-title-protectingward = Ochraná Vizita
buff-desc-protectingward = Jsi chráněn, nějak, před útoky.
## Frenzied
buff-title-frenzied = Šílený
buff-desc-frenzied = Jsi prostoupen nepřirozenou rychlostí a můžeš ignorovat drobná zranění
## Haste
buff-title-hastened = Uspěchaný
buff-desc-hastened = Tvoje pohyby a útoky jsou rychlejší.
## Bleeding
buff-title-bleed = Krvácení
buff-desc-bleed = Způsobuje pravidelné poškození.
## Curse
buff-title-cursed = Prokletí
buff-desc-cursed = Jsi prokletý.
## Burning
buff-title-burn = V Plamenech
buff-desc-burn = Hoříš zaživa
## Crippled
buff-title-crippled = Zmrzačený
buff-desc-crippled = Pohybuješ se jako mrzák, protože tvoje nohy jsou těžce poškozeny.
## Freeze
buff-title-frozen = Zmražen
buff-desc-frozen = Tvé pohyby a útoky jsou zpomaleny.
## Wet
buff-title-wet = Mokrý
buff-desc-wet = Kloužou ti nohy, proto je obtížné zastavit.
## Ensnared
buff-title-ensnared = Polapen
buff-desc-ensnared = Liány ti svazují nohy, takže se nemůžeš hýbat.
## Fortitude
buff-title-fortitude = Opevnění
buff-desc-fortitude = Dokážeš odolat nárazům.
## Parried
buff-title-parried = Odražen
buff-desc-parried = Byl jsi odražen a nyní se pomalu zotavuješ.
## Util
buff-text-over_seconds = více než { $dur_secs } sekund
buff-text-for_seconds = po dobu { $dur_secs } sekund
buff-remove = Klikni pro zrušení
## Probably Deprecaded
buff-title-missing = Chybějící název
buff-desc-missing = Chybějící popis




